public with sharing class TestDataFactory {
     public static Account createSingleAccount(String firstName,String LastName,String recordTypeName)
    {
        Id recTypeId;
        if(String.isNotEmpty(recordTypeName))
        {
             recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        }
        Account acc = new Account();
        String name;
        if(String.isNotEmpty(firstName))
             name = firstName;
        if(String.isNotEmpty(LastName))
            name = name +' '+ LastName;
        if(String.isNotEmpty(recordTypeName))
            acc.RecordTypeId = recTypeId;
        acc.Name = name;
        insert acc;
        return acc;
    }
    public static List<Account> createAccounts(Integer noOfAccounts,String recordTypeName)
    {
        Id recTypeId;
        if(String.isNotEmpty(recordTypeName))
        {
             recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        }
        List<Account> accountList = new List<Account>();
        for(Integer i=0;i<noOfAccounts;i++)
		{
            Account acc = new Account(Name='TestAccount '+i);
            if(String.isNotEmpty(recordTypeName))
             acc.RecordTypeId = recTypeId;
            accountList.add(acc);
        }
        if(accountList.size()>0)
        {
            insert accountList;
        }
        return accountList;
    }

	public static Contact createSingleContact(String firstName,String LastName,String recordTypeName)
    {
        Id recTypeId;
        if(String.isNotEmpty(recordTypeName))
        {
             recTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        }
        Contact con = new Contact();
        if(String.isNotEmpty(firstName))
             con.firstname = firstName;
        if(String.isNotEmpty(LastName))
            con.lastname = LastName;
        if(String.isNotEmpty(recordTypeName))
            con.RecordTypeId = recTypeId;
        insert con;
        return con;
    }
	public static List<Contact> createContacts(Integer noOfContacts,String recordTypeName)
    {
        Id recTypeId;
        if(String.isNotEmpty(recordTypeName))
        {
             recTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        }
        List<Contact> contactList = new List<Contact>();
        for(Integer i=0;i<noOfContacts;i++)
        {
            Contact con = new Contact(LastName='TestContact '+i);
            if(String.isNotEmpty(recordTypeName))
             con.RecordTypeId = recTypeId;
            contactList.add(con);
        }
        if(contactList.size()>0)
        {
            insert contactList;
        }
        return contactList;
    }

	public static Opportunity createSingleOpportunity(String firstName,String LastName,String recordTypeName)
    {
        Id recTypeId;
        if(String.isNotEmpty(recordTypeName))
        {
             recTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        }
		String name;
        Opportunity opp = new Opportunity();
        if(String.isNotEmpty(firstName))
             name = firstName;
        if(String.isNotEmpty(LastName))
            name = name +' '+ LastName;
        if(String.isNotEmpty(recordTypeName))
            opp.RecordTypeId = recTypeId;
        opp.Name = name;
		opp.StageName = 'Prospecting';
		opp.CloseDate = System.today();
        insert opp;
        return opp;
    }
    public static List<Opportunity> createOpportunities(Integer noOfOpportunities,String recordTypeName)
    {
        Id recTypeId;
        if(String.isNotEmpty(recordTypeName))
        {
            recTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        }
        List<Opportunity> oppList = new List<Opportunity>();
        for(Integer i=0;i<noOfOpportunities;i++)
        {
            Opportunity opp = new Opportunity(Name='TestOpportunity '+i,StageName='Prospecting',CloseDate = System.today());
            if(String.isNotEmpty(recordTypeName))
             opp.RecordTypeId = recTypeId;
            oppList.add(opp);
        }
        if(oppList.size()>0)
        {
            insert oppList;
        }
        return oppList;
    }

	public static Lead createSingleLead(String firstName,String LastName,String recordTypeName)
    {
        Id recTypeId;
        if(String.isNotEmpty(recordTypeName))
        {
             recTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        }
        Lead l = new Lead();
        if(String.isNotEmpty(firstName))
             l.firstname = firstName;
        if(String.isNotEmpty(LastName))
            l.lastname = LastName;
        if(String.isNotEmpty(recordTypeName))
            l.RecordTypeId = recTypeId;
        l.Company = 'MTX';
        insert l;
        return l;
    }
    public static List<Lead> createLeads(Integer noOfLeads,String recordTypeName)
    {
        Id recTypeId;
         if(String.isNotEmpty(recordTypeName))
        {
             recTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        }
        List<Lead> leadList = new List<Lead>();
        for(Integer i=0;i<noOfLeads;i++)
        {
           Lead l = new Lead();
            l.LastName ='TestLead'+i;
            l.Company='Test';
            if(String.isNotEmpty(recordTypeName))
             l.RecordTypeId = recTypeId;
            leadList.add(l);
        }
        if(leadList.size()>0)
        {
            insert leadList;
        }
        return leadList;
    }
    public static String ipAddress() {
        
        return Auth.SessionManagement.getCurrentSession().get('SourceIp');
        
        }
    public static Profile getProfileId(String profileName){
            
        Profile pfid= [Select Id from profile where Name=:profileName LIMIT 1]; 
            return pfid;
    }
    public static User createUser(String firstName,String lastName,String profileName,String userName,String email){

        User u=new User();
        if(String.isNotEmpty(firstName))
            u.FirstName = firstName;
        if(String.isNotEmpty(LastName))
            u.LastName= lastName;
        if(String.isNotEmpty(userName))
            u.Username= userName;
        if(String.isNotEmpty(email))
            u.Email= email; 
        if(String.isNotEmpty(profileName))
        {
            Profile p= getProfileId(profileName); 
            u.ProfileId = p.Id;
        }
        insert u;
        return u;

    }
    public static List<User> createUsers(Integer noOfUsers,String profileName)
    {
        Profile p=getProfileId(profileName);
        List<User> userList = new List<User>();
        for(Integer i=0;i<noOfUsers;i++)
        {
                User user = new User();
                user.FirstName = 'Test';
                user.LastName = 'Name';
                user.CompanyName = 'IT Test Company';
                user.MobilePhone = '123-456-7890';
                user.Username = 'testUser'+i;
                user.Email = 'testUser'+i+'@test.com';
         		user.Alias = 'test';
                user.CommunityNickname = 'test1';
                user.TimeZoneSidKey = 'America/New_York';
                user.LocaleSidKey = 'en_US';
                user.EmailEncodingKey = 'UTF-8';
                user.ProfileId = p.Id;
                user.LanguageLocaleKey = 'en_US';
                user.Street = '123 Test St';
                user.City = 'Testcity';
                user.State = 'va';
                user.PostalCode = '23223';
                user.Country = 'USA';
            userList.add(user);
        }
        if(userList.size()>0)
        {
            insert userList;
        }
        return userList;
    }
    public static User createCommunityUser(String firstName,String lastName,String profileName,String userName,String email){

        User communityuser=new User();
        if(String.isNotEmpty(firstName))
           communityuser.FirstName = firstName;
        if(String.isNotEmpty(LastName))
           communityuser.LastName= lastName;
        if(String.isNotEmpty(userName))
           communityuser.Username= userName;
        if(String.isNotEmpty(email))
           communityuser.Email= email; 
        if(String.isNotEmpty(profileName))
        {
            Profile p= getProfileId(profileName); 
            communityuser.ProfileId = p.Id;
        }
        insert communityuser;
        return communityuser;

    }
    public static List<User> createCommunityUsers(Integer noOfUsers,Id conId,String profileName)
    {
        Profile profile = getProfileId(profileName);
        List<User> userList = new List<User>();
        for(Integer i=0;i<noOfUsers;i++)
        {
            User user = new User(alias = 'test'+i, email='test'+i+'@noemail.com',
            emailencodingkey='UTF-8', lastname='Test'+i, languagelocalekey='en_US',
            localesidkey='en_US', profileid = profile.Id, country='United States',IsActive =true,
            ContactId = conId,
            timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
            userList.add(user);
        }
        if(userList.size()>0)
        {
            insert userList;
        }
        return userList;
    }
    public static Group createGroup(String groupName,String groupType){
        
        Group grp = new Group();
        if(String.isNotEmpty(groupName))
          grp.name = groupName;
        if(String.isNotEmpty(groupType))
          grp.Type = groupType; 
        Insert grp; 
        return grp;
    }
    public static  QueuesObject createQueue(Id groupId,String objectType){
        QueuesObject q=new QueuesObject();
        if(String.isNotEmpty(groupId))
            q.queueId=groupId;
        if(String.isNotEmpty(objectType))
            q.sObjectType=objectType;
        insert q;
        return q;
    }
    public static String getUniqueEmailAddress(String firstName, String lastName){
        return firstName + '.' + lastName + '@' + UserInfo.getOrganizationId() + '.com';
    }
    public static feeditem getFeedItem(string body,id parentid,string Type,string visibility)
   {
        return new feeditem(
            body =  body,
            parentid = parentid,
            type = type,
            visibility = visibility
        );
   }
     public static feedcomment getFeedComment(string CommentBody,Id FeedItemId,string commenttype)
   {
        return new feedcomment(
            CommentBody =  CommentBody,
            FeedItemId = FeedItemId,
            commenttype = commenttype
        );
   }
   public static Attachment getAttachment(String attachmentName,String body,String parentId){
       Attachment attach = new Attachment();   	
        attach.Name = attachmentName;
        attach.body = Blob.valueOf(body);
        attach.parentId = parentId;
    
    return attach;
}
public static string getRandomValidPhoneNumber() {
    string result = '',result1='',result2='',result3='';
    while(result1.length() < 3){
        blob privateKey = crypto.generateAesKey(256);
        string randomString = EncodingUtil.base64Encode(crypto.generateMac('hmacSHA512',privateKey,privateKey));
        result1 += randomString.replaceAll('[^0-9]','');
    }
    result1 = result1.substring(0,3);
    while(result2.length() < 3){
        blob privateKey = crypto.generateAesKey(256);
        string randomString = EncodingUtil.base64Encode(crypto.generateMac('hmacSHA512',privateKey,privateKey));
        result2 += randomString.replaceAll('[^0-9]','');
    }
    result2 = result2.substring(0,3);
    while(result3.length() < 4){
        blob privateKey = crypto.generateAesKey(256);
        string randomString = EncodingUtil.base64Encode(crypto.generateMac('hmacSHA512',privateKey,privateKey));
        result3 += randomString.replaceAll('[^0-9]','');
    }
    result3 = result3.substring(0,4);
    result='('+result1+') '+result2+'-'+result3;
    return result;
    
}
}
